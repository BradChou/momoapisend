﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace GoMomoSend
{
    class Program
    {
        private static void Main(string[] args)
        {
            momolog _momolog = new momolog();
            Notification_DAL _Notification_DAL = new Notification_DAL();
            string momosend = "1001";
            _momolog.insertmomolog(momosend, "Start", "OK");

            try
            {
                for (int i = 1; i <= 2; i++)
                {

                    DateTime now = DateTime.Now;
                    TaiwanCalendar taiwanCalendar = new TaiwanCalendar();
                    string text = DateTime.Now.Hour.ToString().PadLeft(2, '0');
                    string text2 = "27365925D8_DR_" + now.ToString("yyyyMMdd") + "_" + text + ".TXT";
                    string value = "D";
                    switch (i)
                    {
                        case 1:
                            text2 = "27365925D8_DR_" + now.ToString("yyyyMMdd") + "_" + text + ".TXT";
                            value = "D";
                            break;
                        case 2:
                            text2 = "27365925R8_RR_" + now.ToString("yyyyMMdd") + "_" + text + ".TXT";
                            value = "R";
                            break;
                    }
                    _momolog.insertmomolog(momosend, text2 + "開始", "OK");
                    using (SqlCommand sqlCommand = new SqlCommand())
                    {
                        sqlCommand.CommandTimeout = 600;
                        //string str = DateTime.Now.AddHours(-1.0).ToString("yyyy-MM-dd HH:00:00");
                        //string str2 = DateTime.Now.ToString("yyyy-MM-dd HH:00:00");
                        //string str3 = "";
                        //str3 = str3 + " and scan.cdate >='" + str + "'";
                        //str3 = str3 + " and scan.cdate <'" + str2 + "'";
                        sqlCommand.Parameters.AddWithValue("@customer_code", "F3500010002");
                        sqlCommand.Parameters.AddWithValue("@DeliveryType", value);
                        sqlCommand.CommandText = $"exec [dbo].[LY_MomoGetSend2] @DeliveryType, @customer_code";
                        DataTable dataTable = dbAdapter.getDataTable(sqlCommand, "JunfuSqlReadOnlyServer");
                        if (dataTable != null && dataTable.Rows.Count > 0)
                        {
                            List<StringBuilder> sb_ins_list = new List<StringBuilder>();
                            StringBuilder sb_temp_ins = new StringBuilder();
                            _momolog.insertmomolog(momosend, "開始SAVETXT", "OK");
                            SaveToTXT(dataTable, ConfigurationManager.AppSettings["Path"] + text2, text2, ref sb_temp_ins, ref sb_ins_list);
                            _momolog.insertmomolog(momosend, "結束SAVETXT", "OK");
                            //upload file to sftp
                            string str4 = "/fse_to_momo/" + text2;
                            SFTPHelper sftpHelper = new SFTPHelper(ConfigurationManager.AppSettings["SFTPPath"], ConfigurationManager.AppSettings["SFTPPort"], ConfigurationManager.AppSettings["SFTPUsername"], ConfigurationManager.AppSettings["SFTPPassword"]);
                            _momolog.insertmomolog(momosend, "開始存TXT", ConfigurationManager.AppSettings["Path"] + text2 + " 到 " + str4);
                            sftpHelper.Upload(ConfigurationManager.AppSettings["Path"] + text2, str4);
                            _momolog.insertmomolog(momosend, "結束存TXT", ConfigurationManager.AppSettings["Path"] + text2 + " 到 " + str4);
                            //新增
                            using (var scope = new TransactionScope(TransactionScopeOption.Required))
                            {
                                //write log to db
                                foreach (StringBuilder item in sb_ins_list)
                                {
                                    if (item.Length > 0)
                                    {


                                        _momolog.insert(item.ToString());

                                        //string commandText = item.ToString();
                                        //_momolog.insertmomolog(momosend, "Insertsql", commandText);

                                        //using (SqlCommand sqlCommand2 = new SqlCommand())
                                        //{
                                        //    try
                                        //    {
                                        //        sqlCommand2.CommandText = commandText;
                                        //        dbAdapter.execNonQuery(sqlCommand2, "JunfuSqlServer");
                                        //    }
                                        //    catch (Exception e)
                                        //    {
                                        //        _momolog.insertmomolog(momosend, "SQL失敗", commandText);
                                        //        _Notification_DAL.Insert("GoMomoSend SQL失敗", e.Message);

                                        //        throw e;
                                        //    }

                                        //}
                                    }
                                }

                                scope.Complete();

                            }


                            //write log to local log file
                            string fileNameWithoutExtension = Path.GetFileNameWithoutExtension(text2);
                            fileNameWithoutExtension += "_log";
                            using (StreamWriter streamWriter = new StreamWriter(ConfigurationManager.AppSettings["Path"] + "log\\" + fileNameWithoutExtension + ".txt", append: true))
                            {
                                streamWriter.Write("\r\nLog : ");
                                streamWriter.WriteLine("{0} {1}  檔案：{2}, 執行總筆數：  {3}", DateTime.Now.ToLongTimeString(), DateTime.Now.ToLongDateString(), Path.GetFileName(text2), dataTable.Rows.Count.ToString());
                                _momolog.insertmomolog(momosend, "解析TXT結束", string.Format("{0} {1}  檔案：{2}, 執行總筆數：  {3}", DateTime.Now.ToLongTimeString(), DateTime.Now.ToLongDateString(), Path.GetFileName(text2), dataTable.Rows.Count.ToString()));
                                streamWriter.Flush();
                                streamWriter.Close();
                            }

                        }
                    }
                    _momolog.insertmomolog(momosend, text2 + "結束", "OK");
                }
            }
            catch (Exception e)
            {
                _momolog.insertmomolog(momosend, "", "Message:" + e.Message + "StackTrace:" + e.StackTrace);
                _Notification_DAL.Insert("GoMomoSend失敗", e.Message);
            }

            _momolog.insertmomolog(momosend, "End", "OK");
        }

        public static void SaveToTXT(DataTable oTable, string FilePath, string FileName, ref StringBuilder sb_temp_ins, ref List<StringBuilder> sb_ins_list)
        {
            momolog _momolog = new momolog();
            string momosend = "1001";
            string str = "";
            TaiwanCalendar taiwanCalendar = new TaiwanCalendar();
            StreamWriter streamWriter = new StreamWriter(FilePath, append: false, Encoding.Default);
            for (int i = 0; i <= oTable.Rows.Count - 1; i++)
            {
                string text = oTable.Rows[i]["check_number"].ToString().Trim();
                string text2 = oTable.Rows[i]["scan_item"].ToString().Trim();
                string text3 = oTable.Rows[i]["exception_option"].ToString();
                string text4 = "";
                if (text2 == "3")
                {
                     text4 = oTable.Rows[i]["arrive_option"].ToString();
                }
                else if (text2 == "5")
                {
                     text4 = oTable.Rows[i]["receive_option"].ToString();
                }          
                string text5 = "";           
                string strVal = oTable.Rows[i]["receive_customer_code"].ToString().Trim();
                strVal = GetBytes(strVal, 0, 10);
                string strVal2 = oTable.Rows[i]["check_number"].ToString().Trim();
                strVal2 = GetBytes(strVal2, 0, 12);
                string strVal3 = "001";
                strVal3 = GetBytes(strVal3, 0, 3);
                string strVal4 = oTable.Rows[i]["order_number"].ToString().Trim();
                strVal4 = GetBytes(strVal4, 0, 32);
                string strVal5 = "";
                strVal5 = GetBytes(strVal5, 0, 2);
                string strVal6 = "";
                strVal6 = GetBytes(strVal6, 0, 32);
                string strVal7 = "";
                strVal7 = GetBytes(strVal7, 0, 32);
                string strVal8 = "";
                strVal8 = GetBytes(strVal8, 0, 3);
                string strVal9 = "";
                strVal9 = GetBytes(strVal9, 0, 64);
                string strVal10 = "";
                strVal10 = GetBytes(strVal10, 0, 32);
                string strVal11 = "";
                strVal11 = GetBytes(strVal11, 0, 32);
                string strVal12 = "";
                strVal12 = GetBytes(strVal12, 0, 3);
                string strVal13 = "";
                strVal13 = GetBytes(strVal13, 0, 64);
                string strVal14 = oTable.Rows[i]["CbmSize"].ToString().Trim();
                strVal14 = GetBytes(strVal14, 0, 2);
                string strVal15 = oTable.Rows[i]["drpieces"].ToString().Trim();
                strVal15 = GetBytes(strVal15, 0, 3);
                string strVal16 = "00000000";
                strVal16 = GetBytes(strVal16, 0, 8);
                string strVal17 = "1";
                strVal17 = GetBytes(strVal17, 0, 1);
                string strVal18 = oTable.Rows[i]["collection_money"].ToString().Trim();
                strVal18 = GetBytes(strVal18, 0, 6);
                string strVal19 = DateTime.Today.ToString("yyyyMMdd");
                strVal19 = GetBytes(strVal19, 0, 8);
                string strVal20 = (oTable.Rows[i]["pickup_date"] != DBNull.Value) ? Convert.ToDateTime(oTable.Rows[i]["pickup_date"]).ToString("yyyyMMddHHmmss") : "";
                strVal20 = GetBytes(strVal20, 0, 14);
                string strVal21 = "";
                strVal21 = GetBytes(strVal21, 0, 4);
                string strVal22 = "";
                strVal22 = GetBytes(strVal22, 0, 3);
                string strVal23 = "";
                strVal23 = GetBytes(strVal23, 0, 4);
                string text6 = "";
                text6 = GetBytes(strVal23, 0, 3);
                string strVal24 = "";
                strVal24 = GetBytes(strVal24, 0, 4);
                string strVal25 = "";
                strVal25 = GetBytes(strVal25, 0, 60);
                string strVal26 = "";
                strVal26 = GetBytes(strVal26, 0, 36);
                string strVal27 = "0";
                strVal27 = GetBytes(strVal27, 0, 1);
                string strVal28 = "";
                strVal28 = GetBytes(strVal28, 0, 8);
                string strVal29 = "";
                strVal29 = GetBytes(strVal29, 0, 6);
                string strVal30 = DateTime.Today.ToString("yyyyMMddHHmmss");
                strVal30 = GetBytes(strVal30, 0, 14);
                string strVal31 = "0";
                switch (text2)
                {
                    case "1":
                        strVal31 = "0";
                        text5 = "配送中";
                        break;
                    case "2":
                        strVal31 = "0";
                        text5 = "配送中";
                        break;
                    case "3":
                        if (new string[4]
                        {
                    "0",
                    "3",
                    "C",
                    "c"
                        }.Contains(text4))
                        {
                            strVal31 = "1";
                            text5 = "已配達";
                        }
                        else if (new string[5]
                        {
                    "4",
                    "7",
                    "8",
                    "9",
                    "49"
                        }.Contains(text4))
                        {
                            strVal31 = "2";
                            text5 = "已配達";
                        }
                        else
                        {
                            strVal31 = "0";
                            text5 = "配送中";
                        }
                        break;
                    case "5":
                        if(new string[1]
                        {
                            "25"
                        }.Contains(text4))
                        {
                            strVal31 = "2";
                            text5 = "已配達";
                        }
                        else
                        {
                            strVal31 = "0";
                            text5 = "配送中";
                        }                    
                        break;
                    case "6":
                        strVal31 = "0";
                        text5 = "配送中";
                        break;
                    case "7":
                        strVal31 = "0";
                        text5 = "配送中";
                        break;
                }
                strVal31 = GetBytes(strVal31, 0, 1);
                string strVal32 = "AO" + text4;
                strVal32 = GetBytes(strVal32, 0, 4);
                string strVal33 = "";
                strVal33 = GetBytes(strVal33, 0, 3);
                string strVal34 = "";
                strVal34 = GetBytes(strVal34, 0, 4);
                string strVal35 = Convert.ToDateTime(oTable.Rows[i]["scan_date"]).ToString("yyyyMMddHHmmss");
                strVal35 = GetBytes(strVal35, 0, 14);
                string strVal36 = "";
                strVal36 = GetBytes(strVal36, 0, 8);
                string strVal37 = "0";
                strVal37 = GetBytes(strVal37, 0, 2);
                string strVal38 = "";
                string text7 = "";
                text7 = text5;
                if (strVal31 == "0")
                {
                    strVal38 = text3;
                    text7 += ((oTable.Rows[i]["exception_option_name"].ToString().Trim() != "") ? oTable.Rows[i]["exception_option_name"].ToString().Trim() : oTable.Rows[i]["arrive_option_name"].ToString().Trim());
                }
                text7 = GetBytes(text7, 0, 12);
                strVal38 = GetBytes(strVal38, 0, 5);
                string strVal39 = "";
                string strVal40 = "";
                if (strVal31 == "2")
                {
                    if (text4 == "25")
                    {
                        text7 = oTable.Rows[i]["receive_option_name"].ToString().Trim();
                    }
                    else
                    {
                        strVal39 = text4;
                        strVal40 = oTable.Rows[i]["arrive_option_name"].ToString().Trim();
                    }                   
                }
                strVal39 = GetBytes(strVal39, 0, 5);
                strVal40 = GetBytes(strVal40, 0, 11);
                str = strVal + strVal2 + strVal3 + strVal4 + strVal5 + strVal6 + strVal7 + strVal8 + strVal9 + strVal10 + strVal11 + strVal12 + strVal13 + strVal14 + strVal15 + strVal16 + strVal17 + strVal18 + strVal19 + strVal20 + strVal21 + strVal22 + strVal23 + text6 + strVal24 + strVal25 + strVal26 + strVal27 + strVal28 + strVal29 + strVal30 + strVal31 + strVal32 + strVal33 + strVal34 + strVal35 + strVal36 + strVal37 + strVal38 + text7 + strVal39 + strVal40;
                str += "\n";
                streamWriter.Write(str);
                str = "";
                string text8 = "";
                text8 = text8 + "INSERT INTO [dbo].[tbMOMOFTPLog] ([logdate] ,[check_number] ,[type],[type_option],[FileName]) VALUES ( getdate() , '" + text + "','" + text2 + "','" + text4 + "','" + FileName + "'); ";
                if (text8 != "")
                {

                    //_momolog.insertmomolog(momosend, "Insertsql", "check_number:" + text + ",scan_item:" + text2 + ",arrive_option:" + text5);

                    sb_temp_ins.Append(text8);
                    if ((i % 200 == 0 && i != 0) || i == oTable.Rows.Count - 1)
                    {
                        sb_ins_list.Add(sb_temp_ins);
                        sb_temp_ins = new StringBuilder();
                    }
                }
            }
            str += "\n";
            streamWriter.Dispose();
            streamWriter.Close();
        }

        private static string GetBytes(string strVal, int sIndex, int length, string fillstring = " ")
        {
            char[] array = strVal.ToCharArray();
            string text = "";
            int num = 0;
            int num2 = 0;
            int num3 = 0;
            while (num2 < sIndex + length)
            {
                if (array.Length > num3)
                {
                    char c = array[num3];
                    UnicodeCategory unicodeCategory = char.GetUnicodeCategory(c);
                    num2 = ((unicodeCategory != UnicodeCategory.OtherLetter) ? (num2 + 1) : (num2 + 2));
                    if (num2 >= sIndex + 1)
                    {
                        num = ((unicodeCategory != UnicodeCategory.OtherLetter) ? (num + 1) : (num + 2));
                        if (num <= length)
                        {
                            text += c;
                        }
                    }
                }
                else
                {
                    text += fillstring;
                    num2++;
                }
                num3++;
            }
            return text;
        }

        public static string SubStr(string a_SrcStr, int a_StartIndex, int a_Cnt)
        {
            Encoding encoding = Encoding.GetEncoding("big5", new EncoderExceptionFallback(), new DecoderReplacementFallback(""));
            byte[] bytes = encoding.GetBytes(a_SrcStr);
            string empty = string.Empty;
            int count = 0;
            int num = 0;
            if (a_Cnt <= 0)
            {
                empty = "";
            }
            if (a_StartIndex + 1 > bytes.Length)
            {
                empty = "";
            }
            else
            {
                if (a_StartIndex + a_Cnt > bytes.Length)
                {
                    count = bytes.Length - a_StartIndex;
                }
                empty = encoding.GetString(bytes, a_StartIndex, count);
            }
            byte[] bytes2 = encoding.GetBytes(empty);
            for (num = bytes2.Length; num < a_Cnt; num++)
            {
                empty += " ";
            }
            return empty;
        }
    }

    internal class dbAdapter
    {
        public static SqlConnection getConnection(string conn = "JunfuSqlServer")
        {
            string connectionString = ConfigurationManager.ConnectionStrings[conn].ToString();
            SqlConnection sqlConnection = new SqlConnection();
            sqlConnection.ConnectionString = connectionString;
            if (sqlConnection.State == ConnectionState.Open)
            {
                sqlConnection.Close();
            }
            sqlConnection.Open();
            return sqlConnection;
        }

        public static DataTable getDataTable(SqlCommand cmd, string connStr = "JunfuSqlServer")
        {
            SqlConnection sqlConnection = null;
            DataTable dataTable = null;
            DataTable dataTable2 = null;
            SqlDataAdapter sqlDataAdapter = null;
            int num = 0;
            try
            {
                sqlConnection = getConnection(connStr);
                dataTable = new DataTable();
                cmd.Connection = sqlConnection;
                sqlDataAdapter = new SqlDataAdapter(cmd);
                num = sqlDataAdapter.Fill(dataTable);
                dataTable2 = new DataTable();
                if (dataTable.Rows.Count > 0 && dataTable.Columns.Count > 0)
                {
                    dataTable2 = dataTable.Clone();
                    for (int i = 0; i < dataTable.Rows.Count; i++)
                    {
                        dataTable2.ImportRow(dataTable.Rows[i]);
                    }
                }
            }
            catch (Exception ex)
            {
                sqlConnection.Close();
                throw ex;
            }
            finally
            {
                sqlConnection.Close();
            }
            return dataTable2;
        }

        public static void execNonQuery(SqlCommand cmd, string connStr = "JunfuSqlServer")
        {
            SqlConnection sqlConnection = null;
            try
            {
                sqlConnection = (cmd.Connection = getConnection(connStr));
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                sqlConnection.Close();
                throw ex;
            }
            finally
            {
                sqlConnection.Close();
            }
        }
    }

}
